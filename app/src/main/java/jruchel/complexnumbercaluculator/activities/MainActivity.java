package jruchel.complexnumbercaluculator.activities;

import android.graphics.Point;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;
import java.util.List;

import jruchel.complexnumbercaluculator.R;
import jruchel.complexnumbers.Calculator;
import jruchel.complexnumbers.Complex;


public class MainActivity extends AppCompatActivity {

    public final double textToScreenRatio = 10;
    public double screenWidth;
    public double screenHeight;

    private List<TextView> textViews;

    //Editable text views
    private EditText realPartTextEdit1;
    private EditText imaginaryPartTextEdit1;
    private EditText realPartTextEdit2;
    private EditText imaginaryPartTextEdit2;

    //Buttons
    private Button powerButton;
    private Button modulusButton;
    private Button argumentButton;
    private Button sumButton;
    private Button subtractButton;
    private Button multiplyButton;
    private Button divideButton;
    private Button clearButton;
    private Button arrowsButton;

    //Variables
    private Complex number1;
    private double realPart1 = 0;
    private double imaginaryPart1 = 0;

    private Complex number2;
    private double realPart2 = 0;
    private double imaginaryPart2 = 0;


    private Point point;
    private int power = 2;
    private String result;

    //Views
    private TextView resultView;

    //Listeners
    //Updates the values of the input numbers as soon as they are altered by the user
    private TextWatcher textWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            setValues();
        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            setValues();
        }

        @Override
        public void afterTextChanged(Editable editable) {
            setValues();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        point = new Point();
        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getRealMetrics(metrics);
        screenHeight = metrics.ydpi;
        screenWidth = metrics.xdpi;

        textViews = new ArrayList<>();

        number1 = new Complex(realPart1, imaginaryPart1);
        number2 = new Complex(realPart2, imaginaryPart2);

        initializeViews();
        setTextSize();
    }

    //Sets the size of all TextViews added to textViews list
    private void setTextSize() {
        //Making the size of the text 1/40 of the screen height (app is locked in portrait mode)
        float size = (float) (screenHeight / textToScreenRatio);

        for (TextView v : textViews) {
            v.setTextSize(size);
        }
    }

    private void initializeViews() {
        //Views-------------------------------------------------------

        //Result text view
        resultView = findViewById(R.id.resultView);
        textViews.add(resultView);

        //View that displays the real part of the number1
        realPartTextEdit1 = findViewById(R.id.realPart);
        realPartTextEdit1.addTextChangedListener(textWatcher);

        //View that displays the imaginary part of the number1
        imaginaryPartTextEdit1 = findViewById(R.id.imaginaryPart);
        imaginaryPartTextEdit1.addTextChangedListener(textWatcher);

        //View that displays the real part of the number1
        realPartTextEdit2 = findViewById(R.id.realPart2);
        realPartTextEdit2.addTextChangedListener(textWatcher);

        //View that displays the imaginary part of the number1
        imaginaryPartTextEdit2 = findViewById(R.id.imaginaryPart2);
        imaginaryPartTextEdit2.addTextChangedListener(textWatcher);

        //Buttons-------------------------------------------------------

        //Module button
        modulusButton = findViewById(R.id.modulusButton);
        modulusButton.setOnClickListener(view -> {
            result = String.valueOf(Calculator.modulus(number1));
            viewResults();
        });

        //Power button
        powerButton = findViewById(R.id.powerButton);
        powerButton.setOnLongClickListener(v -> {
            EditText input = new EditText(MainActivity.this);
            input.setInputType(InputType.TYPE_CLASS_NUMBER);
            new AlertDialog.Builder(MainActivity.this)
                    .setMessage("Please enter the power: ")
                    .setCancelable(false)
                    .setView(input)
                    .setPositiveButton("Ok",
                            (dialog, which) -> {
                                try {
                                    power = Integer.valueOf(input.getText().toString());
                                } catch (NumberFormatException ignored) {
                                }
                            }).show();
            return true;
        });
        powerButton.setOnClickListener(view -> {
            result = String.valueOf(Calculator.power(number1, power));
            viewResults();
        });

        //General argument button
        argumentButton = findViewById(R.id.argumentButton);
        argumentButton.setOnClickListener(view -> {
            result = String.valueOf(Calculator.mainArgument(number1));
            viewResults();
        });

        //Sum button
        sumButton = findViewById(R.id.sumButton);
        sumButton.setOnClickListener(view -> {
            result = Calculator.sum(number1, number2).toString();
            viewResults();
        });

        //Subtract button
        subtractButton = findViewById(R.id.subtractButton);
        subtractButton.setOnClickListener(view -> {
            result = Calculator.subtract(number1, number2).toString();
            viewResults();
        });

        //Multiply button
        multiplyButton = findViewById(R.id.multiplyButton);
        multiplyButton.setOnClickListener(view -> {
            result = Calculator.multiply(number1, number2).toString();
            viewResults();
        });

        //Divide button
        divideButton = findViewById(R.id.divideButton);
        divideButton.setOnClickListener(view -> {
            result = Calculator.divide(number1, number2).toString();
            viewResults();
        });

        //Clear button
        clearButton = findViewById(R.id.clearButton);
        clearButton.setOnClickListener(v -> {
            result = "";
            clearInput();
            viewResults();
        });

        //Arrows button
        arrowsButton = findViewById(R.id.arrowsButton);
        arrowsButton.setOnClickListener(v -> {
            String realPart1 = realPartTextEdit1.getText().toString();
            String imaginaryPart1 = imaginaryPartTextEdit1.getText().toString();

            realPartTextEdit1.setText(realPartTextEdit2.getText());
            imaginaryPartTextEdit1.setText(imaginaryPartTextEdit2.getText());
            realPartTextEdit2.setText(realPart1);
            imaginaryPartTextEdit2.setText(imaginaryPart1);
        });
    }


    //Sets numbers values to the ones currently on input
    private void setValues() {
        try {
            realPart2 = Double.valueOf(realPartTextEdit2.getText().toString());
        } catch (NumberFormatException ex) {
            realPart2 = 0;
        }
        try {
            imaginaryPart2 = Double.valueOf(imaginaryPartTextEdit2.getText().toString());
        } catch (NumberFormatException ex) {
            imaginaryPart2 = 0;
        }
        try {
            realPart1 = Double.valueOf(realPartTextEdit1.getText().toString());
        } catch (NumberFormatException ex) {
            realPart1 = 0;
        }
        try {
            imaginaryPart1 = Double.valueOf(imaginaryPartTextEdit1.getText().toString());
        } catch (NumberFormatException ex) {
            imaginaryPart1 = 0;
        }
        number1 = new Complex(realPart1, imaginaryPart1);
        number2 = new Complex(realPart2, imaginaryPart2);
    }

    private void clearInput() {
        //Clearing the texts
        realPartTextEdit1.setText("");
        imaginaryPartTextEdit1.setText("");
        realPartTextEdit2.setText("");
        imaginaryPartTextEdit2.setText("");
    }

    //Specifies the way results are viewed
    private void viewResults() {
        resultView.setText(result);
    }

}
